import React, { useEffect, useState } from "react";

import {
  Card,
  CardImg,
  CardText,
  CardBody,
  Button,
  Col,
} from "reactstrap";

const Insta = ({item, newLike}) => {

  return (
    <Col xs="4">
      <Card>
        <CardImg
          top
          src={item.imagen}
          alt="Card image cap"
        />
        <CardBody>
          <CardText>
            {item.coment}
          </CardText>
          <Button color="primary" onClick={() => newLike(item.id)} >Like! <span class="badge badge-light">{item.likes}</span></Button>
        </CardBody>
      </Card>
      <br />
    </Col>
  );
};

export default Insta;
